import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router'; 
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { InvoicesService } from './invoices/invoices.service';
import { PostsService } from './posts/posts.service';
import { ProductsService } from './products/products.service';
import { InvoiceFormService } from './invoice-form/invoice-form.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostsComponent } from './posts/posts.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { PostComponent } from './post/post.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';

export const firebaseConfig = {
    apiKey: "AIzaSyDnStmFN5TtWwQkEGT1fzaqWgF2YVFcmXg",
    authDomain: "angular-class-2fe26.firebaseapp.com",
    databaseURL: "https://angular-class-2fe26.firebaseio.com",
    storageBucket: "angular-class-2fe26.appspot.com",
    messagingSenderId: "483390299281"
}

const appRoutes:Routes = [
  {path:'invoices', component:InvoicesComponent},
  {path:'invoice-form', component:InvoiceFormComponent},
  {path:'users', component:UsersComponent},
  {path:'posts', component:PostsComponent},
  {path:'products', component:ProductsComponent},
  {path:'', component:InvoiceFormComponent},
  {path:'**', component:PageNotFoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    PostsComponent,
    UserFormComponent,
    ProductsComponent,
    ProductComponent,
    PostComponent,
    InvoiceComponent,
    InvoicesComponent,
    InvoiceFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)

  ],
  providers: [PostsService ,UsersService, ProductsService,InvoicesService,InvoiceFormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
