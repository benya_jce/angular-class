import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { AngularFire } from 'angularfire2';

@Injectable()
export class PostsService {
  //private _url = 'http://jsonplaceholder.typicode.com/posts';
  postObsurvable;

  getPosts(){
  //return this._http.get(this._url).map(urlposts => urlposts.json()).delay(2000)
  this.postObsurvable = this._af.database.list('/posts').map(
    posts =>{
      posts.map(
        post => {
          post.userName =[];
          for(var u in post.users){
            post.userName.push(
              this._af.database.object('/users/' + u)
            )
          }
        }
      );
      return posts;
    }
  )
  ;

  return this.postObsurvable;
  }
  addPost(post){
    this.postObsurvable.push(post);
  }

  updatePoset(post){
    let key = post.$key;
    let details = {title:post.title, body:post.body }
    this._af.database.object('/posts/'+key).update(details);
  }
  deletePost(post){
    this._af.database.object('/posts/' + post.$key).remove();
  }
  constructor(private _af:AngularFire) { }

}

