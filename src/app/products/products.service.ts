import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AngularFire } from 'angularfire2';
@Injectable()

export class ProductsService {
  productsObservable;

  getProducts()
  {
    this.productsObservable = this._af.database.list('/product').map(
    products =>{
      products.map(
        product => {
          product.categoriesList = this._af.database.object('/category/' + product.categoryId)
            
          
        }
      );
      return products;
    }
  );
    return this.productsObservable;
  }

  deleteProduct(product)
    {
      this._af.database.object('/product/' + product.$key).remove();
    }

  constructor(private _af:AngularFire) { }

}
