import { Component, OnInit } from '@angular/core';
import { ProductsService } from './products.service';


@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;
  properties = [1,2,3,4,6];
  properties2 = [1,2,3,5,6];

  constructor(private _productService:ProductsService) { }

  deleteProd(productToDelete){
    this._productService.deleteProduct(productToDelete);
  }

  ngOnInit() {
    this._productService.getProducts().subscribe(productsData=> this.products = productsData);
   
  }

}
