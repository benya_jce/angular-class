import { Component, OnInit,Output, EventEmitter } from '@angular/core';
import { Invoice } from '../invoice/invoice';
import { NgForm } from '@angular/forms';
import {InvoicesService} from '../invoices/invoices.service';
import { InvoiceFormService } from './invoice-form.service';

@Component({
  selector: 'jce-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
  invoice:Invoice = {name:'', amount:1000};
  lastInvoice:Invoice;
  visible:boolean = false;

 

  onSubmit(form:NgForm)
  {
    console.log(form);
    this._InvoiceFormService.addInvoices(this.invoice);
    this.lastInvoice = Object.assign({}, this.invoice);
    this.visible = true;
    
    this.invoice = {name:'', amount:1000};
  }


  constructor(private _InvoiceFormService:InvoiceFormService) { }

  ngOnInit() {
  }

}
