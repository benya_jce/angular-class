import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class InvoiceFormService {

invoicesObservable = this._af.database.list('/invoice');

addInvoices(invoice){
      this.invoicesObservable.push(invoice);
}

  constructor(private _af:AngularFire) { }

}
