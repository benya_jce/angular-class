/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { InvoiceFormService } from './invoice-form.service';

describe('Service: InvoiceForm', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvoiceFormService]
    });
  });

  it('should ...', inject([InvoiceFormService], (service: InvoiceFormService) => {
    expect(service).toBeTruthy();
  }));
});
