import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
   .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
     .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

  users;
  currentUser;
  isLoading:Boolean = true;

  select(user){
    this.currentUser = user;
  }
  constructor(private _usersService:UsersService) { }
  deleteUser(userToDelete){
    this._usersService.deleteUser(userToDelete);
  }

  addUser(user)
  {
    this._usersService.addUsers(user);
  }

  updateUser(user)
  {
    this._usersService.updateUser(user);
  }

  ngOnInit() {
    this._usersService.getUserFromApi().subscribe(usersData => {this.users = usersData.result; this.isLoading = false; console.log(this.users)});
   // this._usersService.getUsers().subscribe(usersData => {this.users = usersData; this.isLoading = false; console.log(this.users)}); //befor API lesson (22/01/2017)
  }

}
