import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';


@Injectable()
export class InvoicesService {
invoicesObservable;

getInvoices(){ 
    this.invoicesObservable = this._af.database.list('/invoice').delay(2000);
    return this.invoicesObservable;
}

addInvoices(invoice){
      this.invoicesObservable.push(invoice);
}
  constructor(private _af:AngularFire) { }

}
