import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
   styles: [`
   .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
     .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]  
})
export class InvoicesComponent implements OnInit {
  invoises;
  currentInvoice;
  isLoading:boolean = true;


  select(invoice){
    this.currentInvoice = invoice;
  }


  addInvoice(invoice)
  {
    this._invoicesService.addInvoices(invoice);
  }
  
  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
//this._invoicesService.getInvoices().subscribe(invoicesData => this.invoises = invoicesData);
    this._invoicesService.getInvoices().subscribe(invoicesData => {this.invoises = invoicesData; this.isLoading = false;});
  }

}
