import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})

export class PostComponent implements OnInit {
  post:Post;
  origin_post;
  editView:boolean = false;
  editButton = "Edit";
  


  @Output()deleteEvant = new EventEmitter<Post>();
  @Output()updateEvant = new EventEmitter<Post>(); 
  constructor() { }

  postDelete()
  {
    this.deleteEvant.emit(this.post);
  }
  toggleEdit()
  {
   this.editButton == "Save" ? this.postUpdate() : this.origin_post = Object.assign({}, this.post);
   
    this.editView = !this.editView;
    this.editView ? this.editButton = "Save" : this.editButton = "Edit";
  }

  cancelEdit()
  {
    this.editView = false;
    this.editView ? this.editButton = "Save" : this.editButton = "Edit";
    this.post.title = this.origin_post.title;
    this.post.body = this.origin_post.body;
  }

  postUpdate()
  {
    this.updateEvant.emit(this.post);
  }
  
  ngOnInit() {
  }

}