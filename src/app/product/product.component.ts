import { Component, OnInit ,EventEmitter, Output} from '@angular/core';
import {Product} from './product'

@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['poduct','category']
})
export class ProductComponent implements OnInit {
  poduct:Product;

  @Output()deleteEvant = new EventEmitter<Product>();

sendDelete(){
    this.deleteEvant.emit(this.poduct);
  }

  constructor() { }

  ngOnInit() {
  }

}
